package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {

    @Test
    void should_say_number_when_FizzBuzz_say_given_input_normal_number() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(1);
        assertEquals("1", actual);
    }

    @Test
    void should_say_Fizz_when_FizzBuzz_say_given_input_is_3() {
        Integer number = 3;
        String expect = "Fizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_Fizz_when_FizzBuzz_say_given_input_is_6() {
        Integer number = 6;
        String expect = "Fizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_Buzz_when_FizzBuzz_say_given_input_is_5() {
        Integer number = 5;
        String expect = "Buzz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_Buzz_when_FizzBuzz_say_given_input_is_10() {
        Integer number = 10;
        String expect = "Buzz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_FizzBuzz_when_FizzBuzz_say_given_input_is_15() {
        Integer number = 15;
        String expect = "FizzBuzz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_Whizz_when_FizzBuzz_say_given_input_is_7() {
        Integer number = 7;
        String expect = "Whizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_Whizz_when_FizzBuzz_say_given_input_is_14() {
        Integer number = 14;
        String expect = "Whizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_FizzWhizz_when_FizzBuzz_say_given_input_is_21() {
        Integer number = 21;
        String expect = "FizzWhizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_BuzzWhizz_when_FizzBuzz_say_given_input_is_35() {
        Integer number = 35;
        String expect = "BuzzWhizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);
    }

    @Test
    void should_say_FizzBuzzWhizz_when_FizzBuzz_say_given_input_is_105() {
        Integer number = 105;
        String expect = "FizzBuzzWhizz";
        FizzBuzz fizzBuzz = new FizzBuzz();
        String actual = fizzBuzz.say(number);
        assertEquals(expect, actual);

    }

}
