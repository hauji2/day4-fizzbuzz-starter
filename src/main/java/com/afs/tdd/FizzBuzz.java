package com.afs.tdd;

public class FizzBuzz {

    public String say(Integer number) {
        StringBuilder result = new StringBuilder();

        boolean isDividedByThree = number % 3 == 0;

        boolean isDividedByFive = number % 5 == 0;

        boolean isDividedBySeven = number % 7 == 0;

        if (isDividedByThree) {
            result.append("Fizz");
        }

        if (isDividedByFive) {
            result.append("Buzz");
        }

        if (isDividedBySeven) {
            result.append("Whizz");
        }

        if (result.isEmpty()) {
            return number.toString();
        }

        return result.toString();
    }

}